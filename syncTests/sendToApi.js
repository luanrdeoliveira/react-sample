const { v4: uuidv4 } = require('uuid');

export default async function sendToApi (data) {
    axios({
      method: 'POST',
      url: 'https://5b1j5a5kji.execute-api.us-east-1.amazonaws.com/default/test-report-api',
      data: {
          TableName: "test-reports",
          Item: {
            id: uuidv4(),
            data: data.date,
            status: data.status,
            link_s3: data.linkS3
          }        
      }    
    });
  }