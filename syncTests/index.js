const sendToApi = require('./sendToApi');
const uploadToS3 = require('./uploadToS3');

const failures = require('../cypress/report/mochawesome.json').stats.failures;
const date = Date.now();

main();

const main = async () => {
    const linkS3 = await uploadToS3('../mochawesome.html');

    const data = {
        date: date,
        status: failures == 0 ? "success" : "failed",
        link_s3: linkS3
    }

    await sendToApi(data);
}

