const fs = require('fs');
const AWS = require('aws-sdk');
require('dotenv/config');

const BUCKET_NAME = "test-results-react";
const IAM_USER_KEY = process.env.IAM_ACCESS_KEY;
const IAM_USER_SECRET = process.env.IAM_SECRET_ACCESS_KEY;
const objectKey = new Date();

const s3bucket = new AWS.S3({
  accessKeyId: IAM_USER_KEY,
  secretAccessKey: IAM_USER_SECRET
});

export default function uploadToS3(fileName) {
  const readStream = fs.createReadStream(fileName);

  const params = {
    Bucket: BUCKET_NAME,
    Key: objectKey,
    Body: readStream
  };

  return new Promise((resolve, reject) => {
    s3bucket.putObject(params, function(err, data) {
      readStream.destroy();
      const s3Link = `https://test-results-react.s3.amazonaws.com/${objectKey}`
      if (err) {
        return reject(err);
      }            
      return resolve(s3Link);
    });
  });
}






